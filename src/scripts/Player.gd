extends KinematicBody2D

const MaxSpeed = 100
const Acceleration = 300
const Friction = 200

var Motion = Vector2.ZERO

func _physics_process(delta):
	var input_vector = get_input_vector()
	apply_movement(input_vector, delta)
	apply_friction(input_vector, delta)
	Motion = move_and_slide(Motion)

func get_input_vector():
	var input_vector = Vector2.ZERO
	input_vector.x = Input.get_action_strength("move_right") - Input.get_action_strength("move_left")
	input_vector.y = Input.get_action_strength("move_down") - Input.get_action_strength("move_up")
	return input_vector.normalized()

func apply_movement(input_vector, delta):
	if input_vector != Vector2.ZERO:
		Motion = Motion.move_toward(input_vector * MaxSpeed, Acceleration * delta)

func apply_friction(input_vector, delta):
	if input_vector == Vector2.ZERO:
		Motion = Motion.move_toward(Vector2.ZERO, Friction * delta)